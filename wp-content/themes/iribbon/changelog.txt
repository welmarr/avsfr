iRibbon Changelog
****************************

Version 1.0.28 (2-21-13)
-Added placeholder for widget class.
-Social icon fix.
-Changed default Twitter account.
-Repositioned wp-footer.

Version 1.0.27 (2-14-13)
-Fixed notice - Undefined offset: 1.

Version 1.0.26 (2-6-13)
-Media uploader fix and filters.

Version 1.0.25 (1-25-13)
-Fix image upload

Version 1.0.24 (1-23-13)
-Printf call to _e.
-Meta class fix.

Version 1.0.23 (1-14-13)
-Sidebar fix.

Version 1.0.22 (1-6-13)
-Error page translation.

Version 1.0.21 (12-29-12)
-URI variable.

Version 1.0.20 (12-22-12)
-Removed title from search form.
-SSL compatibility.

Version 1.0.19 (12-16-12)
-Callout action added translation function.

Version 1.0.18 (12-6-12)
-3.5 fixes.

Version 1.0.17 (11-26-12)
-Enabled pinch to zoom.
-Slide fix for pages.
-Broken links fix.
-Added action for callout element.

Version 1.0.16 (11-19-12)
-Feed title fix.

Version 1.0.15 (11-12-12)
-Footer-actions fix.

Version 1.0.14 (11-07-12)
-Added password protection for elements.
-Removed extra comment div.
-Added clear div for overflow issue.

Version 1.0.13 (10-31-12)
-Google font fix.

Version 1.0.12 (10-23-12)
-Quick edit fix.

Version 1.0.11 (10-15-12)
-Quick edit fix.

Version 1.0.10 (10-08-12)
-Archive actions fix.

Version 1.0.9 (10-01-12)
-Printf call to echo variable.

Version 1.0.8 (09-25-12)
-Setup pie.php file.

Version 1.0.7 (09-18-12)
-Changed how translations are laid out.

Version 1.0.6 (09-10-12)
-Fixed language files.
-Updated style copy.

Version 1.0.5 (09-04-12)
-Fixed responsive slider in IE and Firefox.
-Added Bootstrap to read me txt.
-Changed read me text for licenses.
-Fixed echo in core functions.

Version 1.0.4 (08-14-12)
-.Org fixes.

Version 1.0.3 (07-16-12)
-Fixed iframe issue.
-Removed extra template options.

Version 1.0.2 (07-10-12)
-Prefix fix
-Twitter embed fix.
-Other required fixes to get theme approved. 

Version 1.0.1 (06-24-12)
-Initial release.

